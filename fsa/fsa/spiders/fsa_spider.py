# -*- coding: utf-8 -*-
"""
Created on Thu May 30 21:18:17 2013

@author: Vathy
"""

# from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from fsa.items import FsaItem
from unidecode import unidecode
                     
# Function to get the municipality
def get_city( fsa ):
    """
    Extracts the city (Municipality) from HTML on Wikipedia
    based on FSA code.
    fsa (input) is an XPATH tag.
    """
    # Try to get the city
    # Formatting can be very inconsistent across tables
    city = fsa.select('span/b/a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/i/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/b/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('i/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('br/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('text()').extract()
               
    return city

# Declare StartUrl and PatternToFollow as constants
STARTURL = "http://en.wikipedia.org/wiki/List_of_postal_codes_in_Canada"
PATTERN_TO_FOLLOW = "en.wikipedia.org/wiki/List_of_._postal_codes_of_Canada"


class FsaSpider(CrawlSpider):
    '''
    Spider to extract FSA Codes for most Canadian cities.
    '''
    name = "fsa"
    allowed_domains = ["wikipedia.org"]
    start_urls = [ STARTURL ]
    
    rules = (
        Rule(
        SgmlLinkExtractor( allow = PATTERN_TO_FOLLOW ),
        'parse_fsa', 
        follow = True,
        ),
    )
    
    def parse_fsa(self, response):
        '''
        Function to to extract both FSA Code and the
        corresponding city.
        '''
        # Append final results to this list
        items = []
        # self.log( 'A response from %s just arrived!' % response.url )
        hxs = HtmlXPathSelector(response)
        allcodes = hxs.select('//td')
        for fsa in allcodes:
            # Initiate class to gather results
            item = FsaItem()
            # Get fsa and city
            fsacode = fsa.select('b/text()').extract()
            # Skip if there is no fsa code
            if fsacode == '' or fsacode == [] : 
                continue
            city = get_city( fsa )
            # Skip if fsa code is not in use or not assigned
            if city == [ u'Not assigned' ] or city == [ u'Not in use' ] :
                continue
            city = [ unidecode(x) for x in city ] 
            item[ 'fsa' ] = fsacode
            item[ 'city' ] = city
            items.append(item)
            # print fsacode, city
            
        # Return all
        return items
