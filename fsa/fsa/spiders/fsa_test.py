# -*- coding: utf-8 -*-
"""
Created on Mon Jun 03 21:50:03 2013

@author: Vathy
"""


# from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from fsa.items import FsaItem
from unidecode import unidecode

                  
# Function to get the municipality
def get_city( fsa ):
    """
    Extracts the city (Municipality) from HTML on Wikipedia
    based on FSA code.
    fsa (input) is an XPATH tag.
    """
    # Try to get the city
    # Formatting can be very inconsistent across tables
    city = fsa.select('span/b/a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/i/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('span/b/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('a/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('i/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('br/text()').extract()
    if city == '' or city == [] :
        city = fsa.select('/text()').extract()
           
    return city
    
# Function to get the fsacode
def get_fsacode( fsa ):
    """
    Extracts the city (Municipality) from HTML on Wikipedia
    based on FSA code.
    fsa (input) is an XPATH tag.
    """
    # Try to get the city
    # Formatting can be very inconsistent across tables
    fsacode = fsa.select('b/text()').extract()
    if fsacode == '' or fsacode == [] :
        fsacode = fsa.select('span/b/text()').extract()
    if fsacode == '' or fsacode == [] :
        fsacode = fsa.select('/text()').extract()
           
    return fsacode
    
# Declare StartUrl and PatternToFollow as constants
STARTURL = "http://en.wikipedia.org/wiki/List_of_postal_codes_in_Canada"
PATTERN_TO_FOLLOW = "en.wikipedia.org/wiki/List_of_._postal_codes_of_Canada"

class FsaSpider(CrawlSpider):
    '''
    Spider to extract FSA Codes for most Canadian cities.
    '''
    name = "fsa_test"
    allowed_domains = ["wikipedia.org"]
    start_urls = [ STARTURL  ]
    
    rules = (
        Rule(
        SgmlLinkExtractor( allow = PATTERN_TO_FOLLOW ),
        'parse_fsa', 
        follow = True,
        ),
    )
    
    def parse_fsa(self, response):
        '''
        Function to to extract both FSA Code and the
        corresponding city.
        '''
        # Append final results to this list
        items = []
        hxs = HtmlXPathSelector(response)
        allcodes = hxs.select('//td')
        for fsa in allcodes:
            # Initiate class to gather results
            item = FsaItem()
            # Get fsa and city
            fsacode = get_fsacode( fsa )
            if fsacode == '' or fsacode == [] : 
                continue
            city = get_city( fsa )
            if city == [ u'Not assigned' ] or city == [ u'Not in use' ] :
                continue
            city = [ unidecode(x) for x in city ]        
            # Add to scrapy item
            item[ 'fsa' ] = fsacode
            item[ 'city' ] = city
            items.append(item)
            
        return items
