# Scrapy settings for fsa project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'fsa'

SPIDER_MODULES = ['fsa.spiders']
NEWSPIDER_MODULE = 'fsa.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'fsa (+http://www.yourdomain.com)'
