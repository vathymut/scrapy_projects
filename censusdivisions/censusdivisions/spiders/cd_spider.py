# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 08:34:30 2013

@author: Vathy M. Kamulete
"""

from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.spiders import CrawlSpider
from scrapy.http import FormRequest, Request
from censusdivisions.items import CensusdivisionsItem
from unidecode import unidecode
from scrapy import log

def get_primarycdinfo( row ):
    """
    Extracts the Census code and the Census name from statscan
    based on FSA code.
    row (input) is an XPATH tag.
    """
    censusname = row.select('text()').extract() 
    censuscode = row.select('a/text()').extract() 
    # Clean up census name
    censusname = ''.join(censusname).strip()
    LastIdx = censusname.find('[')
    censusname = unidecode( censusname[0:LastIdx] )
    # Clean up census code
    censuscode = ''.join( censuscode ).strip()
    censuscode = [ unidecode(x) for x in censuscode.split() ] 
    censuscode = ', '.join( censuscode )
    # Return results
    return censusname, censuscode
    
DictToSubmit = { 'CII_SuperBtn' : 'Search',
                 'lang' : 'eng', 
                 'p2' : '-1', 
                 'pattern' : '282-0054', 
                 'typeValue' : '1' }

class CdSpider(CrawlSpider):
    name = "cd_statscan"
    allowed_domains = ["statcan.gc.ca"]
        
    def start_requests(self):
        return [FormRequest("http://www5.statcan.gc.ca/cansim/a03", 
                            formdata = DictToSubmit, 
                            callback = self.logged_in) ]    

    def logged_in(self, response):
        # Append final results to this list
        # items = []
        # Process the response
        hxs = HtmlXPathSelector(response) 
        # Get all rows of interest
        allrows = hxs.select("//th[contains(@class, 'Left')]")
        # print allrows
        for row in allrows:
            # Initiate class to gather results
            item = CensusdivisionsItem()
            # Get censuscode and censusname
            censusname, censuscode = get_primarycdinfo( row )
            # Add to scrapy item
            if not censuscode == [ ] :
                item[ 'censusname' ] = censusname
                item[ 'censuscode' ] = censuscode
                # print censusname, censuscode
                # Extract links and visit pages
                for url in row.select('a/@href').extract():
                    # print url
                    request = Request(url, callback=self.get_subdivions)
                    request.meta['item'] = item
                    yield request
                
        
    def get_subdivions(self, response):
        '''
        Function to extract the census division code from url.
        '''
        # self.log( 'A response from %s just arrived!' % response.url )
        item = response.meta['item']
        # Process the response
        hxs = HtmlXPathSelector(response) 
        cdcode = hxs.select("//td[contains(@scope, 'row')]/a/text()").extract()
        cdcode = [ unidecode(x) for x in cdcode ] 
        cdcode = ', '.join( cdcode )
        item[ 'cdcode' ] = cdcode
        yield item