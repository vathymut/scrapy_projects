# Scrapy settings for censusdivisions project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'censusdivisions'

SPIDER_MODULES = ['censusdivisions.spiders']
NEWSPIDER_MODULE = 'censusdivisions.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'censusdivisions (+http://www.yourdomain.com)'
