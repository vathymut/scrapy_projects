# Scrapy settings for brokers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'brokers'

SPIDER_MODULES = ['brokers.spiders']
NEWSPIDER_MODULE = 'brokers.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'brokers (+http://www.yourdomain.com)'
