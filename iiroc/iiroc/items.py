# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class iirocItem(Item):
    # define the fields for your item here like:
    # name = Field()
    firm_name = Field( )
    alt_names = Field( )
    category = Field( )
    renewal_dates = Field( )
    revocation_dates = Field( )
