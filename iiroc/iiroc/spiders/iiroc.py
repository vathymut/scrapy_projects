# -*- coding: utf-8 -*-
"""
Created on Sun Jun 30 00:26:32 2013

@author: Vathy M. Kamulete (For Jason Allen)
"""
from __future__ import absolute_import
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.spiders import CrawlSpider
#from scrapy.http import FormRequest, Request
from scrapy.http import TextResponse 
from iiroc.items import iirocItem
from unidecode import unidecode

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from exceptions import UnicodeDecodeError
from exceptions import IndexError

#from selenium.webdriver.common.keys import Keys

import time
from contextlib import contextmanager

@contextmanager
def start_exceptions_manager():
    '''
    Create context manager to handle exceptions.
    '''
    print ' Now within the context manager...'
    try:
        yield
    except ( NoSuchElementException, 
             TimeoutException,
             ElementNotVisibleException,
             UnicodeDecodeError, 
             IndexError ):
        handle_exceptions()
    finally:
        print ' Now exiting the context manager...'
    
def handle_exceptions():
    '''
    Handle exceptions raised if no results found. 
    '''
    print 'There was an error'
    pass

def get_historical_info( browser ):
    '''
    Parse historical data for the firm.
    '''
    # Extract the first link from the table of query results
    firstlink = browser.find_element_by_xpath( LINK_XPATH )
    firstlink.click( )
    time.sleep(2.5)
    
    # Extract the firm historical information link
    histinfo = browser.find_element_by_id( HIST_INFO_ID )
    histinfo.click( )
    time.sleep(2.5)

def get_query_results( browser, firm_name ):
    '''
    Submit query for each firm.
    Input:
        browser, selenium rendered browser (resilient to Javascript)
        firm_name, the firm name passed as a string
    '''
    # Get the historical data for the firm  
    # Add firm name in search field
    firmnamefield = browser.find_element_by_id( FIRMNAME_INPUT_ID )
    firmnamefield.send_keys( firm_name )
    
    # Uncheck active box
    activeboxcheck = browser.find_element_by_id( ACTIVE_BOX_ID )
    activeboxcheck.click( )
    time.sleep(2)
    
    # Check Historical prior to September 28, 2009 box
    histboxcheck = browser.find_element_by_id( HIST_BOX_ID )
    histboxcheck.click( )
    time.sleep(2)
    
    # Check search by firm name box
    firmboxcheck = browser.find_element_by_id( FIRM_BOX_ID )
    firmboxcheck.click( )
    time.sleep(2)
    
    # Click on search 
    search = browser.find_element_by_id( SEARCH_ID )
    search.click( )
    time.sleep(2)

def get_scrapy_response( browser ):
    '''
    Create response for scrapy to parse
    '''
    # convert html to "nice format"
    text_html = browser.page_source.encode('utf-8')
    html_str = str(text_html)

    # this is a hack that initiates a "TextResponse" object 
    # (taken from the Scrapy module)
    # StackOverflow link:
    #   http://stackoverflow.com/a/17518842/1965432
    resp_for_scrapy = TextResponse('none', 200, {}, html_str,
                                   [], None)

    # takes a "TextResponse" object and feeds it to a scrapy function 
    # which will convert the raw HTML to a XPath document tree
    return HtmlXPathSelector( resp_for_scrapy )
    
def filter_list_of_dates( list_of_dates ):
    '''
    Filter the list of dates.
    It returns a list of lists. Each nested list has two elements.
    The first element is the list of Registration Date. 
    The second is the list of Revocation Date.
    '''
    # Initiate empty list
    new_list = []
    registration_dates = list_of_dates[0::2]
    revocation_dates = list_of_dates[1::2]
    for idx in range( len(registration_dates) ):
        new_list.append( [ registration_dates[idx], revocation_dates[idx] ] )  
    return new_list

def filter_category( list_of_categories ):
    '''
    Filter the list of categories and keep only records if
    category is not an empty string. Investment Dealer should
    be in these categories. 
    '''
    # Initiate empty list
    new_list = []
    for cat in list_of_categories:
        if cat != "":
            new_list.append( cat )
    return new_list

def clean_list( list_of_items, func_to_apply = None ):
    '''
    Clean the list of items. It encapsulates some common procedures 
    for this project.
    '''
    list_of_items = [ unidecode(item) for item in list_of_items ]
    list_of_items = [ " ".join( item.split() ) for item in list_of_items]
    if func_to_apply is not None:
        list_of_items = func_to_apply( list_of_items )
    return list_of_items

def match_date_to_cat( list_of_cats, list_of_dates ):
    '''
    Group together the category for Investment Dealer with its equivalent
    registration and revocation dates. 
    '''
    match_list = []
    cats_no = len(list_of_cats)
    dates_no = len(list_of_dates)
    max_idx = min( [cats_no, dates_no] )
    for idx in range( cats_no ):
        item = [ list_of_cats[idx], list_of_dates[idx] ]
        match_list.append( item )           
    return match_list
            

# For testing purposes
LIST_OF_FIRMS_TO_TEST = ['Assante Capital Management Ltd.', 
                         'Bieber Securities Inc.',
                         'Brault, Guy, O\'Brien Inc.', 
                         'First Delta Securities Inc.',
                         'Placements Banque Nationale inc.' ] 
             
# Paramaters for the query
SEARCHURL = "http://www.securities-administrators.ca/nrs/nrsearch.aspx?id=850"
FIRMNAME_INPUT_ID = "ctl00_bodyContent_txtFirmName"
ACTIVE_BOX_ID = "ctl00_bodyContent_chkActive"
HIST_BOX_ID = "ctl00_bodyContent_chkHistPrior"
FIRM_BOX_ID = "ctl00_bodyContent_rdFirm"
SEARCH_ID = "ctl00_bodyContent_ibtnSearch"
HIST_INFO_ID = "ctl00_bodyContent_lbtnShowFirmHistorical"
ALT_NAMES_XPATH = "//*[@id='ctl00_bodyContent_lbl_firm_name']/text()"
LINK_XPATH = "//table[@id='ctl00_bodyContent_gv_frm_list']/tbody/tr/td/a[1]"
CAT_XPATH = "//tr[@class='catrow']/td[@class='special']/text()"
DATES_XPATH = "//tr[@class='catrow']/td[@class='special']/span/div/text()"

FILEPATH1 = 'C:\\Users\\Vathy\\Documents\\iiroc\\KIS-ListOfCompaniesPart1.txt'
FILEPATH2 = 'C:\\Users\\Vathy\\Documents\\iiroc\\KIS-ListOfCompaniesPart2.txt'
FILEPATH3 = 'C:\\Users\\Vathy\\Documents\\iiroc\\KIS-ListOfCompaniesPart3.txt'
FILEPATH4 = 'C:\\Users\\Vathy\\Documents\\iiroc\\FirmsNotInCSA.txt'


class IirocSpider(CrawlSpider):
    '''
    A spider to extract iiroc dealer information off of the Canadian
    Securities Administrator website.
    '''
    name = "dealers"
    allowed_domains = [ "securities-administrators.ca" ]
    start_urls = [ SEARCHURL  ]
    
    def __init__(self):
        '''
        Start selenium to handle javascript
        '''
        CrawlSpider.__init__(self)
        self.verificationErrors = []
        self.selenium = webdriver.Firefox()
    
    def parse(self, response):
        '''
        Submit queries for all firms.
        '''
        # Start empty list
        all_items_results = []
        file_to_read = open( FILEPATH4, 'r')
        list_of_firm_names = file_to_read.readlines()
        list_of_firm_names = [ firm.strip() for firm in list_of_firm_names ]
        # Close file to be sure
        file_to_read.close()
        # LIST_OF_FIRMS_TO_TEST
        # list_of_firm_names
        for idx, firm_name in enumerate(list_of_firm_names):
            # Track the number of queries perfomed
            print idx
            # webdriver rendered page
            driver = self.selenium
            driver.get( response.url )
            if driver:
                # Wait for javascript to load in Selenium
                time.sleep(3) 
                
            with start_exceptions_manager():    
                get_query_results(browser = driver, firm_name = firm_name )
                get_historical_info(browser = driver )
                # Get html response
                hxs = get_scrapy_response( browser = driver )
                # select names
                alt_names = hxs.select( ALT_NAMES_XPATH ).extract()
                alt_names = [ unidecode(name) for name in alt_names ]
                # Select categories
                cat_list = hxs.select( CAT_XPATH ).extract()
                cat_list = clean_list( list_of_items = cat_list, 
                                       func_to_apply =  filter_category )
                # print cat_list   
                # Select dates 
                dates_list = hxs.select( DATES_XPATH ).extract()
                dates_list = clean_list( list_of_items = dates_list, 
                                         func_to_apply =  \
                                         filter_list_of_dates)
                # print dates_list
                # Extract dates for investment dealers only
                dealers_list = match_date_to_cat( list_of_cats = cat_list,
                                                  list_of_dates = \
                                                  dates_list )
                # print dealers_list
                for dealer in dealers_list:
                    item = iirocItem( )
                    item[ 'firm_name' ] = firm_name
                    item[ 'alt_names' ] = alt_names
                    item[ 'category' ] = dealer[0]
                    item[ 'renewal_dates' ] = dealer[1][0]
                    item[ 'revocation_dates' ] = dealer[1][1]
                    # Print to monitor progess
                    # Append to list
                    all_items_results.append( item )
                                   
        # Return all
        return all_items_results