# Scrapy settings for iiroc project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'iiroc'

SPIDER_MODULES = ['iiroc.spiders']
NEWSPIDER_MODULE = 'iiroc.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'iiroc (+http://www.yourdomain.com)'
