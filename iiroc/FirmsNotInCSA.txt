﻿Acadian Securities Inc.
Alpha ATS L.P.
Arcadia Wealth Management Inc.
Barclays Global Investors Services Canada Limited
Barret Capital Management Inc.
Begg Securities Limited
Bekhor Securities Canada Limited
Berner & Company Inc.
Brault, Guy, O`Brien Inc.
Brenark Securities Ltd.
Burns Fry Limited
Cassels Blaikie & Co. Limited
Castleton Commodities Canada Securities LP
Charlton Securities Limited
Chouinard, McNamara Inc.
Citibank Canada Securities Limited
Connor Clark & Company Ltd.
CT Securities International Inc.
CWM Capital Inc.
D & B Internat Securities Inc.
Dahlman Rose & Company Canada, Inc.
Daiwa Securities Canada Limited
Davo Investments (Canada) Limited
Deragon Langlois Inc.
Derivative Services Inc.
Desjardins, Deragon, Langlois, Ltee
DFI Securities Inc.
Drexel Burnham Lambert Canada Inc.
E*TRADE Institutional (Canada) Corporation
Essex Capital Management Limited
Euro Canadian Securities Limited
First Canadian Capital Management Inc.
Forbes & Walker Securities Limited
Fort House Inc.
Fraser Dingman Limited
Galiam Securities Canada Corp.
GMP Private Client Corp.
Gordon Private Client Corporation
Great Western Securities Inc.
Guardinvest Securities Inc.
GW Welkin Capital Corporation of Canada
J.D. Mack Limited
John Graham & Company Limited
KingsGate Securities Limited
KPLV Securities Inc.
Leduc & Associés Valeurs mobilières inc.
Liberty Wealth Management Inc.
LVM Canada Ltée
Lynch Investments Limited
MacDougall, Meyer Inc.
Market Street Investment House Inc.
Marlow Group Securities Inc.
Matrix Financial Corporation
Maxima Capital Inc.
MBT Global Trading, LP
Midland Walwyn Canada Inc.
Mirabaud Asset Management (Canada) Inc.
Morgan Wilshire Securities (Canada) Inc.
Nomura Canada Inc.
Ouimet Hubbs Inc.
Pensec Inc.
Placements La Laurentienne (valeurs mobilières) inc.
Porthmeor Securities
Pro-Genesis Securities Inc.
Progressive Wealth Management LLP
Pucetti Farrell Capital Partners
R.J. Beatty Financial Corp.
Ransom Financial Corporation
Réflexion Capital Inc.
Republic Securities Canada Inc.
Robert Caldwell Capital Corporation
Roy Bertrand Lemay Tessier inc.
Sands Brothers Canada Ltd.
Sanyo Securities Canada Ltd.
Schroder Canada Limited
Scotia Bond Company Limited
Security Trading Inc.
Sheridan Securities Inc.
Shorcan ATS Limited
St. James Securities Inc.
Tassé & Associés, Limitée
Tegwaan Securities Corp.
Teraxis Securities Inc.
The Financial Centre Securities Corporation
The Nikko Securities Co. Canada, Ltd.
UBS Securities (Canada) Limited
United Services Advisors Securities Inc.
Valeurs mobilières CMA inc.
Valeurs mobilières Dubeau ltée.
Wallace Smith Securities Inc.
Walwyn Stodgell Cochran Murray Ltd.
Yamaichi International (Canada) Limited
